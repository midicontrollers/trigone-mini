# TrigOne Mini MIDI Controller

## Overview

The TrigOne Mini MIDI Controller is a compact, handcrafted device designed for the discerning audio professional or enthusiast. Built with precision and care, it features a unique wooden design with a high-quality rotary encoder and a switch, providing intuitive control over your music production software.

This controller is perfect for those who need a portable and stylish solution for adjusting volume, effects, and other parameters in real-time. The TrigOne Mini operates on CircuitPython, ensuring a flexible and user-friendly programming experience.

## Features

- Handcrafted from quality wood
- Rotary encoder with acceleration feature for increased control sensitivity
- Single switch with dual MIDI CC functionality
- USB connectivity for easy integration with music production environments
- Customizable and open-source code available on GitLab

## Installation

1. Ensure you have CircuitPython installed on your Controller Pico. Visit [CircuitPython Installation Guide](https://circuitpython.org/board/raspberry_pi_pico/) for detailed instructions.
2. Connect the TrigOne Mini to your computer via USB.
3. Copy the provided `code.py` file to the root directory of your Controller Pico.
4. The device should be recognized by your computer as a MIDI device.

## Quick Start

1. Turn the rotary encoder to adjust MIDI CC 7 (typically volume control) with acceleration sensitivity.
2. Press the switch to toggle MIDI CC 80 (assigned to a general-purpose control) between values of 0 and 127.
3. The TrigOne Mini is now ready to be mapped to your preferred DAW or MIDI-compatible software.

## Code

The code for the TrigOne Mini is designed to be straightforward and modifiable. Visit the [TrigOne Mini GitLab Repository](#gitlab-link-placeholder) to access the code, detailed comments, and update notes.

## Support

For questions, troubleshooting, or to report an issue, please visit the [Issues section](#gitlab-issues-section-placeholder) of our GitLab repository.

## Contributing

Contributions to the TrigOne Mini MIDI Controller project are welcome! If you have suggestions or improvements, please fork the repository and submit a pull request.

## License

The TrigOne Mini MIDI Controller code and design are licensed under the Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) License. You are free to share and adapt this software as long as you provide appropriate credit, indicate if changes were made, and do not use the material for commercial purposes.

For full details of the license, please visit [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/).



---

Thank you for choosing the TrigOne Mini MIDI Controller. We hope it inspires your musical creativity and enhances your production workflow.

