"""
Circuitpython Code for the TrigOne Mini MIDI Controller by triggerpointed

Rotary sends on CC 7,
Switch is on CC 80 (Off = 0, On = 127)

Author: Christian Neyens / triggerpointed
Version: 1.2.1
"""
__version__ = '1.2.1'
__AUTHOR__ = 'Christian Neyens (triggerpointed@icloud.com)'

import board
import time
import digitalio
import rotaryio
import usb_midi
import adafruit_midi

from adafruit_midi.control_change import ControlChange

# Initialize MIDI
midi = adafruit_midi.MIDI(midi_out=usb_midi.ports[1], out_channel=0)

# Initialize the rotary encoder
encoder = rotaryio.IncrementalEncoder(board.GP4, board.GP3)
last_position = None

rotary_ccval = 0
last_rotary_ccval = rotary_ccval

rotary_accel = 1
accel_factor = 1.3
last_rotary_accel_time = 0
accel_debounce_delay = 0.07

# Initialize the switch
switch = digitalio.DigitalInOut(board.GP2)
switch.direction = digitalio.Direction.INPUT
switch.pull = digitalio.Pull.DOWN

switch_state = switch.value
last_switch_state = switch_state

cc_state = switch_state
last_cc_state = cc_state

last_debounce_time = 0  # Tracks the last time switch state changed
debounce_delay = 0.05  # 50 milliseconds debounce delay

while True:
    # Save the starting time
    current_time = time.monotonic()
    # Rotary 1
    position = encoder.position
    if last_position is None:
        last_position = position  # Initialize last_position with the current position
    if position != last_position:
        last_rotary_accel_time = current_time	# Save the time when the knob was turned    
        if position > last_position:
            rotary_ccval = int(rotary_ccval + rotary_accel + 0.5)
            if rotary_ccval > 127:
                rotary_ccval = 127
        if position < last_position:
            rotary_ccval = int(rotary_ccval - rotary_accel - 0.5)
            if rotary_ccval < 0:
                rotary_ccval = 0
        last_position = position
        rotary_accel = rotary_accel * accel_factor	# accelerate value changes
    
    if rotary_ccval != last_rotary_ccval:
        if rotary_ccval > last_rotary_ccval:
            rotary_step = 1
        else:
            rotary_step = -1
        # step trough all Midi values for the change, to have a smooth change in the DAW
        for ccval in range(last_rotary_ccval,  rotary_ccval + rotary_step, rotary_step):
            if ccval < 0:
                ccval = 0
            if ccval > 127:
                ccval = 127
            midi.send(ControlChange(7, ccval))	# Send MIDI CC 7 (Volume)
        last_rotary_ccval = rotary_ccval
    # End of Code for Rotary 1

    # Switch 1
    if current_time - last_rotary_accel_time > accel_debounce_delay:
        rotary_accel = rotary_accel * 0.5
        if rotary_accel < 1:
            rotary_accel = 1	# Accel of 2 feels better. For precise settings, 1 is necessary though
            
    switch_state = switch.value
    if switch_state != last_switch_state:
        # Switching (instead of On when pressed)
        if (current_time - last_debounce_time) > debounce_delay:
            if switch_state:
                if cc_state:
                    cc_state = False
                    cc_value = 0
                else:
                    cc_state = True
                    cc_value = 127
            if last_cc_state != cc_state:
                midi.send(ControlChange(80, cc_value))	# Send MIDI CC 80 (General Purpose)
                last_cc_state = cc_state          
            last_debounce_time = current_time
            last_switch_state = switch_state
        else:	# only for DEbug
            # print(f'Debouncing - current time: ', current_time)
            pass
    # End of Code for Switch 1

